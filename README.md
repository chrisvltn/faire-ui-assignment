## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.<br>
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

It will also automatically run `npm run server`, which starts a proxy to Faire's API, due to CORS issues.
