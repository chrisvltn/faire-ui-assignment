import { Brand } from "./Proto";

export interface Maker {
	id: string
	name: string
	shortDescription: string
	isFavorite: boolean
	mainImage: string
	images: Image[]
	raw: Brand
}

interface Image {
	url: string
	height: number
	width: number
}
