export enum SortOrder {
	ASC = 'ASC',
	DESC = 'DESC',
}

export enum MakerValue {
	ECO_FRIENDLY = 'ECO_FRIENDLY',
	HAND_MADE = 'HAND_MADE',
	NOT_SOLD_ON_AMAZON = 'NOT_SOLD_ON_AMAZON',
	CHARITABLE = 'CHARITABLE',
	MADE_IN_USA = 'MADE_IN_USA',
}

export enum LeadTime {
	THREE_OR_LESS_DAYS = 'THREE_OR_LESS_DAYS',
	SIX_OR_LESS_DAYS = 'SIX_OR_LESS_DAYS',
	NINE_OR_LESS_DAYS = 'NINE_OR_LESS_DAYS',
	FOURTEEN_OR_LESS_DAYS = 'FOURTEEN_OR_LESS_DAYS',
}

export enum SearchMakersWithFiltersSortBy {
	NAME = 'NAME',
	SCORE = 'SCORE',
	FIRST_ACTIVE_AT = 'FIRST_ACTIVE_AT',
	MINIMUM = 'MINIMUM',
}

export enum Type {
	PRODUCT = 'PRODUCT',
	PACKAGING = 'PACKAGING',
	LIFESTYLE = 'LIFESTYLE',
	DEFAULT_BRAND = 'DEFAULT_BRAND',

	// A squared image for a brand with a light color pallete.
	LIGHT_TILE = 'LIGHT_TILE',

	// A squared image for a brand with a medium color pallete.
	MEDIUM_TILE = 'MEDIUM_TILE',

	// A squared image for a brand with a dark color pallete.
	DARK_TILE = 'DARK_TILE',

	// A landing page image for brand partnerships with a light color palette.
	LIGHT_WHOLESALE_PORTAL_LANDING = 'LIGHT_WHOLESALE_PORTAL_LANDING',

	// A landing page image for brand partnerships with a dark color palette.
	DARK_WHOLESALE_PORTAL_LANDING = 'DARK_WHOLESALE_PORTAL_LANDING',

	// A signup page image for brand partnerships with a light color palette.
	LIGHT_WHOLESALE_PORTAL_SIGNUP = 'LIGHT_WHOLESALE_PORTAL_SIGNUP',

	// A signup page image for brand partnerships with a dark color palette.
	DARK_WHOLESALE_PORTAL_SIGNUP = 'DARK_WHOLESALE_PORTAL_SIGNUP',

	// Can be the hero image for a displayable entity. Each entity can have at most one Hero image.
	HERO = 'HERO',

	MOBILE = 'MOBILE',
	DESKTOP = 'DESKTOP',

	// Used to show that the type of this image is represented as a tag
	TAGGED = 'TAGGED',
}

export enum State {
	// The brand was newly created.
	NEW = 'NEW',

	// The brand was rejected from Faire.
	REJECTED = 'REJECTED',

	// THe onboarding form was sent to the brand.
	ONBOARDING_FORM_SENT = 'ONBOARDING_FORM_SENT',

	// The brand has started the onboarding form.
	ONBOARDING_FORM_STARTED = 'ONBOARDING_FORM_STARTED',

	// The brand has completed the onboarding form and we have their catalog.
	ONBOARDING_FORM_COMPLETED = 'ONBOARDING_FORM_COMPLETED',

	// The brand has had at least one product uploaded.
	PROFILE_STARTED = 'PROFILE_STARTED',

	// The brand has all of their products uploaded.
	ALL_PRODUCTS_UPLOADED = 'ALL_PRODUCTS_UPLOADED',
	PROFILE_IN_DESIGN_REVIEW = 'PROFILE_IN_DESIGN_REVIEW',
	PROFILE_IN_FINAL_REVIEW = 'PROFILE_IN_FINAL_REVIEW',
	PROFILE_ACTIVATED = 'PROFILE_ACTIVATED',
	WAITLISTED = 'WAITLISTED',
}

export interface Brand {
	token: string

	/** Unique identifier of the brand that the brand gets to pick, used in the subdomain for their elevate page */
	token_alias: string

	/** If true this Brand is owned by Faire. */
	is_internal: boolean

	/**
	 * Generally all brands should be active. Unless we've determined that a brand has issues
	 * that prevent it from being listed on IF. When a brand is inactive, all its products
	 * are not displayed to customers, even if the product themselves are active.
	 */
	active: boolean

	name: string
	short_description: string
	description: string
	url: string
	categories: string[]
	/**
	 * These are categories where the brand does poorly so we derank the brand in those categories to
	 * preserve relevance.
	 */
	images: Image[]
	facebook_handle: string
	twitter_handle: string
	instagram_handle: string
	youtube_handle: string
	pinterest_handle: string
	/** This is a derived property from firstOrderMinimumAmountCents and reorderMinimumAmountCents. */
	minimum_order_amount_cents: number
	/** The minimum order amount when a retailer is placing their first order with a brand. */
	first_order_minimum_amount_cents: number
	reorder_minimum_amount_cents: number

	/**
	 *  This defines where the brand is headquartered. Note that this can be different
	 * from both where its products are made and where its products ship from.
	 */
	from: string
	/**
	 * Made in information. For products made in the USA, we also specify the state and city,
	 * if available. TODO: Move madeIn to an enumerated type.
	 */
	made_in: string
	made_in_state: string
	made_in_city: string

	/** This is where the brand is based. Typically where the founders live or are head quartered. */
	based_in: string
	based_in_state: string
	based_in_city: string

	/** The brand is made using materials or techniques good for the environment. If null, unknown. */
	eco_friendly: boolean
	/** The brand dedicates some of its proceeds to a charitable effort. If null, unknown. */
	charitable: boolean
	/** The brand is hand made. If null, unknown. */
	hand_made: boolean
	/** The brand is women owned. If null, unknown. */
	women_owned: boolean
	/** The brand's products are organic. If null, unknown. */
	organic: boolean
	/** The brand's products are small batch. If null, unknown. */
	small_batch: boolean

	/** Whether the brand is sold on Amazon, either by the brand itself or third parties. */
	sold_on_amazon: boolean

	/** Read-only (generated by the server). */
	made_in_usa_description: string

	/** Read-only (generated by the server). */
	hand_made_description: string

	/** Writable. If the maker is charitable, what kind of charity do they do */
	charitable_description: string

	/** Writabe. If the maker is eco friendly, what is eco friendly about it */
	eco_friendly_description: string

	/** The brand is local to the location of the querying retailer. If null, unknown. */
	local: boolean

	/** A quote from the owner of this brand. */
	owner_quote: any

	/** An image, typically of the owner shown close to the owner's quote. */
	story_image: Image

	/** A url that links to either a Vimeo or a YouTube video. */
	video_url: string

	/** A list of instagram images. */
	social_media_images: Image[]

	badges: any[]

	/**
	 * Some brands have intrinsic lead times (even when not back-ordered).
	 * Handmade brands for example can take four weeks to fulfil.
	 */
	lead_time_weeks: number

	/**
	 * The average lead time for this brand.
	 */
	lead_time_days: number

	/**
	 * When displaying a range for the lead time, this is the lower bound of that range.
	 */
	lower_bound_lead_time_days: number

	/**
	 * When displaying a range for the lead time, this is the upper bound of that range.
	 */
	upper_bound_lead_time_days: number

	/** Whether the current customer has favorited this brand. */
	favorite: boolean

	/**
	 * This is a summarized view of the brand's likes or followers across all of its social media
	 * platforms.
	 */
	likes: number
	/** Best guess on when the brand was created. Sometimes approximate. */
	creation_year: number

	/** A squared image representing the brand to be used in tiles and in the app. */
	squared_image: Image

	/** The best image to display for a brand/category pair. */
	squared_category_images: { [key: string]: Image }

	/** Email image and description are used for weekly emails to retailers. */
	email_hero_image: Image
	email_description: string

	/** Number of followers across the main social networks. */
	facebook_followers: number
	instagram_followers: number
	twitter_followers: number

	/** If not null, this is a reward amount to show for this brand. */
	first_order_reward_amount_cents: number

	/** If not null, this is the default amount that will be applied if the retailer is eligible. */
	default_reward_amount_cents: number

	/** If not null, this is a discount percentage to show for this brand. */
	first_order_discount_percentage: number

	/** If not null, this is the default discount for this brand, if the retailer is eligible. */
	default_discount_percentage: number

	/**
	 * If not empty, this is the list of terms available for this brand and the current retailer.
	 * Empty is there is no retailer in the session, the frontend must re-fetch this brand before
	 * going through flows that require this information.
	 */
	available_terms: any[]

	/** Is this the first order for this brand and retailer */
	is_first_order: boolean

	/** Maximum amount that can be ordered on first order with free returns for this brand. */
	first_order_returns_limit_cents: number

	zip_code_protection_enabled: boolean

	/** Timestamp of last renewal of Shopify access for this Brand. */
	shopify_access_renewed_at: number

	/** Whether we should sync with the brands's POS, independent of any valid access tokens. */
	pos_syncing_enabled: boolean

	shopify_accesstoken: string

	shopify_shop_name: string
	first_active_at: number
	active_products_count: number
	last_product_added_at: number
	has_new_products: boolean

	memberships: any[]

	elevate_commission_bps: number

	ship_with_indigo_fair: boolean

	/**
	 * If the brand is within the Boom exclusivity window and the retailer in the current session
	 * has boom, this flag is true.
	 */
	is_boom_exclusive: boolean

	/** If the brand allows scheduled orders or not */
	allows_scheduled_orders: boolean

	// Next tag is 110

	/** Whether the brand is "new" for some reasonable definition of new to mean "newly added". */
	is_new: boolean

	/** The drop dead date after which the brand cannot fulfill thanksgiving orders. */
	thanksgiving_cutoff_date: number

	/** The drop dead date after which the brand cannot fulfill christmas orders. */
	christmas_cutoff_date: number

	/** If this flag is set, the lead time estimates are not computed automatically. */
	override_lead_time_estimates: boolean

	ship_with_faire_preference: any

	vacation_start_date: number

	vacation_end_date: number

	vacation_banner_text: string

	no_consumer_resale: boolean

	accepts_returns: boolean

	return_orders_discount_bps: number

	brand_reviews_summary: any

	version: number
	can_set_packaging_and_handling_costs: boolean
	shipping_label_printing_preference: any
}

export interface Color {
	name: string
	rgb: string
}

export interface ProductOption {
	token: string

	/** Whether this product option is active. */
	active: boolean

	/** The option is marked as deleted, it won't be returned in any API anymore. */
	deleted: boolean

	/** The group name is the name of the option "type". Examples are Colors, Scents, Size, etc. */
	group_name: string

	/**
	  * Options can be nested to support more complex option. For example an iPhone case can be Blue
	  * and for iPhone 7+.
	  */
	parent_token: string

	/** The name of this product option. In case of color it could be the name of the color. */
	name: string

	/** The SKU number assigned to this product option by the brand. */
	brand_code: string

	description: string

	/** An color derived from the name of the color of the option. */
	color: Color

	/**
	  * By default the wholesale price should come from the product wholesale price.
	  * This field allows overrides in the rare cases when the wholesale price is different.
	  */
	wholesale_price_cents: number

	/** This works very similarly to the wholesale price. */
	retail_price_cents: number

	/** Also known as case size or case quantity, this is the unit in which this option ships. */
	unit_multiplier: number

	images: Image[]

	backordered_until_date: number

	/** Whether this product option offers testers. */
	has_testers: boolean

	/** If set, only allow adding to cart at most this quantity. */
	available_units: number

	/** The measurements of this specific option. */
	measurements: any

	/** The SKU number assigned to this product option by the brand for testers. */
	tester_brand_code: string

	product_token: string

	version: number
}

export interface Product {
	token: string

	/** To be used only internal by the server, not for external APIs. */
	id: number

	/** We expose the version for caching purposes. */
	version: number

	brand_token: string

	/**
	 * If the brandToken was overwritten because of Maker Mix and Match,
	 * this keeps the original token. In all other cases, it's just a copy of the brand token.
	 */
	original_brand_token: string

	/**
	  * Only active products are displayed to customer. Note that active products that belong to
	  * inactive brands are not displayed to customers.
	  */
	active: boolean
	name: string
	short_description: string
	description: string
	wholesale_price_cents: number
	retail_price_cents: number

	/** If this product offers testers, this is the price of each tester (regardless of option). */
	tester_price_cents: number

	/** Also known as case size or case quantity. This is the unit size that this product ships in. */
	unit_multiplier: number
	url: string
	measurements: any
	categories: string[]
	options: ProductOption[]
	amazon_review_count: number
	amazon_url: string
	lowest_online_price_cents: number
	images: Image[]
	manual_recommendation: boolean
	recommenders: any[]

	/** A score we use for globally ranking products. */
	score: number

	/** Used to experiment with different scoring. */
	new_score: number

	/** Whether the customer has favorited this product. */
	favorite: boolean

	/**
	 * This product was tagged as a best seller for its maker. This is not a global best seller flag,
	 * just local to the maker.
	 */
	maker_best_seller: boolean

	/**
	 * If set, this is used to sort this product in the maker profile page.
	 * The ranking still lists maker best sellers first, then it sorts by this field.
	 */
	maker_relative_position: number

	backordered_until_date: number

	/** Whether the product is "new" for some reasonable definition of new to mean "newly added". */
	is_new: boolean

	/**
	 * By default all products are returnable but some of our products are marked as non returnable
	 * (customer items, perishables, etc.)
	 */
	returnable: boolean

	/** Whether or not the product is a custom one-off product. */
	custom: boolean

	created_at: number
	updated_at: number

	// Tags are out of order, next tag is 34.
}


export interface Image {
	token: string
	width: number
	height: number
	sequence: number
	url: string
	type: Type
	/** A distinct list of tags associated with this image */
	tags: string[]

	/**
	 * This type allows us to distinguish between image types in all contexts
	 * where that might be ambiguous. For example, when a product has multiple types of images
	 * attached to it.
	 */

}


export interface Category {
	name: string
	sub_categories: Category[]
	/**
	 * Product based categories are categories that are attached to a taxonomy property
	 * of a certain product (Home Decor, Candles, Umbrellas).
	 */
	is_product_based: boolean

	/**
	 * If this is set, the frontend will deep link from the category list view to the products
	 * that the maker has with that category. This was first implemented for the "Holidays"
	 * category so that we could support landing on the "Holidays" category page within a maker.
	 */
	link_to_maker_category: string

	/**
	* Featured categories are given a prominent position in the sidebar
	*/
	is_featured: boolean

	token: string
}


export interface PaginationData {
	page_number: number
	page_size: number
	page_count: number
	total_results: number
}

export type Optional<T> = {
	[K in keyof T]?: T[K]
}

export interface SearchMakersWithFiltersRequest {
	name?: string
	states?: string[]
	maker_values?: Optional<MakerValue>[]
	category?: string
	maximum_maker_minimum_cents?: number
	maker_minimum_thresholds?: number[]
	lead_time?: Optional<LeadTime>
	pagination_data?: Optional<PaginationData>
	sort_order?: Optional<SortOrder>
	sort_by?: Optional<SearchMakersWithFiltersSortBy>
	max_products_per_brand?: number
}

export interface SearchMakersWithFiltersResponse {
	maker_values_preview: { [key: string]: number }
	based_in_state_preview: { [key: string]: number }
	categories_preview: { [key: string]: number }
	maker_minimums_preview: { [key: number]: number }
	lead_time_preview: { [key: string]: number }
	brands: Brand[]
	pagination_data: PaginationData
	sort_order: SortOrder
	sort_by: SearchMakersWithFiltersSortBy
	brand_products: { [key: string]: ProductsForCategory }
	/**
	 * Unique ID for the request. Must be supplied to /click-track endpoint
	 * whenever a result from this response is clicked by the user.
	 */
	request_id: string
}


export interface ProductsForCategory {
	products: Product[]
	/** If null, the above products are for the brand in general rather than a specific category. */
	category: string
}
