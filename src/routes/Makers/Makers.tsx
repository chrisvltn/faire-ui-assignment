import React from 'react'
import FilterList from '../../components/Filter/FilterList/FilterList';
import MakerList from '../../components/Makers/MakerList/MakerList';
import MakerListPagination from '../../components/Makers/MakerListPagination/MakerListPagination';

const Makers = () =>
	<>
		<div className="fl w-25">
			<FilterList />
		</div>
		<div className="fl w-75">
			<MakerList />
			<MakerListPagination />
		</div>
	</>

export default Makers
