import { createStore, combineReducers, applyMiddleware } from 'redux'
import { makers, MakerState } from './reducers/makers'
import thunkMiddleware from 'redux-thunk'
import { modal, ModalState } from './reducers/modal';

export type StoredState = {
	makers: MakerState,
	modal: ModalState,
}

export default createStore(
	combineReducers({
		makers,
		modal,
	}),
	applyMiddleware(
		thunkMiddleware,
	),
)
