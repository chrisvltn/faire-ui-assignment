import { ReactElement } from "react";

export function toggleModal(show: boolean, content: ReactElement | null): ModalAction {
	return {
		type: ModalActionTypeEnum.TOGGLE_MODAL,
		show,
		content,
	}
}

export type ModalAction = {
	type: ModalActionTypeEnum.TOGGLE_MODAL
	show: boolean
	content: ReactElement | null
}

export enum ModalActionTypeEnum {
	TOGGLE_MODAL = 'TOGGLE_MODAL',
}
