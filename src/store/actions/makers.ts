import { Maker } from "../../entities/Maker";
import { SearchMakersWithFiltersResponse } from "../../entities/Proto";
import { Dispatch } from "redux";
import { StoredState } from "../store";
import Axios from "axios";
import { MakerState } from "../reducers/makers";

export function addFilter(key: string, value: string | object) {
	return function (dispatch: Function, getState: () => StoredState) {
		const state = getState()
		let filters = { ...state.makers.filters }
		switch (key) {
			case 'states':
			case 'maker_values':
			case 'maker_minimum_thresholds':
				filters = {
					...filters,
					[key]: [...filters[key], value],
				}
				break
			default:
				filters = {
					...filters,
					[key]: value,
				}
		}

		if (key != 'pagination_data') {
			filters.pagination = { page_number: 1 }
			dispatch({ type: ActionTypeEnum.UPDATE_PAGE, value: 1 })
		}

		return dispatch(updateFilter(filters))
	}
}

export function removeFilter(key: string, value: string) {
	return function (dispatch: Function, getState: () => StoredState) {
		const state = getState()
		let filters = { ...state.makers.filters }

		switch (key) {
			case 'states':
			case 'maker_values':
			case 'maker_minimum_thresholds':
				const filter = filters[key] as string[]
				filters = {
					...filters,
					[key]: filter.filter(item => item != value),
				}
				break
			default:
				filters[key] = value
		}

		return dispatch(updateFilter(filters))
	}
}

let debounce: NodeJS.Timeout
let finish: Function
let result = new Promise(r => finish = r)
export function updateFilter(filters: MakerState['filters']) {
	return async function (dispatch: Dispatch) {
		return new Promise(resolve => {
			clearTimeout(debounce)
			debounce = setTimeout(async () => {
				dispatch({ type: ActionTypeEnum.IS_FILTER_LOADING, value: true })
				const { data } = await Axios.post<SearchMakersWithFiltersResponse>('/api/search/makers-with-filters', filters)
				dispatch({ type: ActionTypeEnum.IS_FILTER_LOADING, value: false })
				dispatch({ type: ActionTypeEnum.UPDATE_PAGE_BOUNDARY, value: data.pagination_data.page_count })

				const makers = data.brands.filter(brand => brand.active).map(brand => ({
					id: brand.token,
					name: brand.name,
					shortDescription: brand.short_description,
					isFavorite: false,
					mainImage: brand.squared_image && brand.squared_image.url,
					images: brand.images.map(image => ({
						url: image.url,
						width: image.width,
						height: image.height,
					})),
					raw: brand,
				}))

				dispatch(setFilters(filters))
				finish(dispatch(updateMakers(makers)))
				result = new Promise(r => finish = r)
			}, 300)

			resolve(result)
		})
	}
}

export function updateMakers(makers: Maker[]): MakerAction {
	return {
		type: ActionTypeEnum.MAKERS_UPDATE,
		makers,
	}
}

export function setFilters(filters: MakerState['filters']): MakerAction {
	return {
		type: ActionTypeEnum.SET_FILTERS,
		filters,
	}
}

export function updatePage(currentPage: number): MakerAction {
	return {
		type: ActionTypeEnum.UPDATE_PAGE,
		value: currentPage,
	}
}

export function updatePageBoundary(maxPages: number): MakerAction {
	return {
		type: ActionTypeEnum.UPDATE_PAGE_BOUNDARY,
		value: maxPages,
	}
}

export type MakerAction = {
	type: ActionTypeEnum.MAKERS_UPDATE
	makers: Maker[]
} | {
	type: ActionTypeEnum.IS_FILTER_LOADING
	value: boolean
} | {
	type: ActionTypeEnum.UPDATE_PAGE | ActionTypeEnum.UPDATE_PAGE_BOUNDARY
	value: number
} | {
	type: ActionTypeEnum.SET_FILTERS
	filters: MakerState['filters']
}

export enum ActionTypeEnum {
	MAKERS_UPDATE = 'MAKERS_UPDATE',
	IS_FILTER_LOADING = 'IS_FILTER_LOADING',
	UPDATE_PAGE = 'UPDATE_PAGE',
	UPDATE_PAGE_BOUNDARY = 'UPDATE_PAGE_BOUNDARY',
	SET_FILTERS = 'SET_FILTERS',
}
