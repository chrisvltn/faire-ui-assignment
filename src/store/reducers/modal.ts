import { ReactElement } from "react";
import { ModalActionTypeEnum, ModalAction } from "../actions/modal";

export type ModalState = {
	show: boolean
	content: ReactElement | null
}

const defaultState: ModalState = {
	show: false,
	content: null,
}

export function modal(state: ModalState = defaultState, action: ModalAction): ModalState {
	switch (action.type) {
		case ModalActionTypeEnum.TOGGLE_MODAL:
			return {
				...state,
				show: action.show,
				content: action.content,
			}
	}

	return state
}

