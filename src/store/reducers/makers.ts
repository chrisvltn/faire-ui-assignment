import { Maker } from "../../entities/Maker";
import { ActionTypeEnum, MakerAction } from "../actions/makers";

export type MakerState = {
	loading: boolean
	filters: {
		[key: string]: string | string[] | undefined | object
	}
	pagination: {
		currentPage: number
		maxPages: number
	}
	data: Maker[]
}

const defaultState: MakerState = {
	loading: true,
	filters: {
		name: undefined,
		states: [],
		maker_values: [],
		category: undefined,
		maximum_maker_minimum_cents: undefined,
		maker_minimum_thresholds: [],
		lead_time: undefined,
		pagination_data: undefined,
		sort_order: undefined,
		sort_by: undefined,
		max_products_per_brand: undefined,
	},
	pagination: {
		currentPage: 1,
		maxPages: 1,
	},
	data: [],
}

export function makers(state: MakerState = defaultState, action: MakerAction): MakerState {
	switch (action.type) {
		case ActionTypeEnum.MAKERS_UPDATE:
			return {
				...state,
				data: action.makers,
			}
		case ActionTypeEnum.IS_FILTER_LOADING:
			return {
				...state,
				loading: action.value,
			}
		case ActionTypeEnum.SET_FILTERS:
			return {
				...state,
				filters: action.filters,
			}
		case ActionTypeEnum.UPDATE_PAGE:
			return {
				...state,
				pagination: {
					...state.pagination,
					currentPage: action.value,
				},
			}
		case ActionTypeEnum.UPDATE_PAGE_BOUNDARY:
			return {
				...state,
				pagination: {
					...state.pagination,
					maxPages: action.value,
				},
			}
	}

	return state
}

