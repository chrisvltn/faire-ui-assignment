import React, { Component } from 'react'
import axios from 'axios'
import { Category, SearchMakersWithFiltersRequest } from '../../../entities/Proto';
import { connect, MapStateToProps } from 'react-redux';
import { withRouter, RouteComponentProps } from 'react-router-dom';
import { addFilter, removeFilter } from '../../../store/actions/makers'
import FilterItem from '../FilterItem/FilterItem';
import FilterDropdown from '../FilterDropdown/FilterDropdown';
import { MakerState } from '../../../store/reducers/makers';
import { StoredState } from '../../../store/store';

type State = {
	loading: boolean
	filterGroups: FilterGroup[]
}

export type FilterGroup = {
	name: string
	key: keyof SearchMakersWithFiltersRequest
	unique: boolean
	filters: Filter[]
}

export type Filter = {
	id: string
	name: string
	active: boolean
	preview?: number
	subItems: Filter[]
}

type Props = MappedProps & RouteComponentProps & StateProps

class FilterList extends Component<Props, State> {
	state: State = {
		loading: true,
		filterGroups: [
			{
				name: 'Marker',
				key: 'maker_values',
				unique: false,
				filters: [
					{
						id: 'ECO_FRIENDLY',
						name: 'Eco Friendly',
						active: false,
						subItems: [],
					},
					{
						id: 'HAND_MADE',
						name: 'Hand Made',
						active: false,
						subItems: [],
					},
					{
						id: 'NOT_SOLD_ON_AMAZON',
						name: 'Not sold on Amazon',
						active: false,
						subItems: [],
					},
					{
						id: 'CHARITABLE',
						name: 'Charitable',
						active: false,
						subItems: [],
					},
					{
						id: 'MADE_IN_USA',
						name: 'Made in USA',
						active: false,
						subItems: [],
					},
				]
			},
			{
				name: 'Lead Time',
				key: 'lead_time',
				unique: true,
				filters: [
					{
						id: 'THREE_OR_LESS_DAYS',
						name: '3- days',
						active: false,
						subItems: [],
					},
					{
						id: 'SIX_OR_LESS_DAYS',
						name: '6- days',
						active: false,
						subItems: [],
					},
					{
						id: 'NINE_OR_LESS_DAYS',
						name: '9- days',
						active: false,
						subItems: [],
					},
					{
						id: 'FOURTEEN_OR_LESS_DAYS',
						name: '14- days',
						active: false,
						subItems: [],
					},
				]
			},
		],
	}

	componentDidMount() {
		this.syncFilters()
		this.loadCategories()

		this.setState(state => {
			const filterGroups = [...state.filterGroups]
			filterGroups.forEach(group => {
				if (this.props.activeFilters) {
					const item = this.props.activeFilters[group.key]
					if (item)
						group.filters.forEach(function checkActive(filter) {
							filter.active = item == filter.id || (Array.isArray(item) && item.some((v: any) => v == filter.id))
							filter.subItems.forEach(checkActive)
						})
				}
			})

			return {
				...state,
				filterGroups,
			}
		})
	}

	syncFilters() {
		const query = this.queryToFilterRequest(this.props.history.location.search)
		Object.entries(query)
			.forEach(([key, value]) => {
				if (Array.isArray(value))
					value.forEach(v => this.props.addFilter(key as keyof SearchMakersWithFiltersRequest, v))
				else
					this.props.addFilter(key as keyof SearchMakersWithFiltersRequest, value)
			})
	}

	async loadCategories() {
		const { data: categories } = await axios.get<Category[]>('/api/category/new')

		const filterGroups = [...this.state.filterGroups].filter(group => group.key != 'category')

		const __this = this

		filterGroups.unshift({
			name: 'Categories',
			key: 'category',
			unique: true,
			filters: categories.map(function toFilterItem(cat: Category): Filter {
				return {
					id: cat.name,
					name: cat.name,
					active: (__this.props.activeFilters || {}).category == cat.name,
					subItems: cat.sub_categories.map(toFilterItem)
				}
			})
		})

		this.setState({
			loading: false,
			filterGroups,
		})
	}

	toggleActiveFilter(groupId: FilterGroup['key'], filterId: Filter['id']) {
		const filterGroups = [...this.state.filterGroups]
		const group = filterGroups.find(group => group.key == groupId)

		if (!group) return

		if (this.props.activeFilters) {
			const key = groupId
			const filterValue = this.props.activeFilters[key]

			if (Array.isArray(filterValue)) {
				if (filterValue.indexOf(filterId) > -1)
					this.props.removeFilter(key, filterId)
				else
					this.props.addFilter(key, filterId)
			} else {
				if (filterValue == filterId)
					this.props.removeFilter(key, filterId)
				else
					this.props.addFilter(key, filterId)
			}
		}

		group.filters = group.filters.map(function toFilterItem(filter: Filter): Filter {
			return {
				...filter,
				active: filter.id == filterId ? !filter.active : (group.unique ? false : filter.active),
				subItems: filter.subItems.map(toFilterItem),
			}
		})

		this.setState({ filterGroups })
		this.updateQueryParams(filterGroups)
	}

	queryToFilterRequest(queryString: string): SearchMakersWithFiltersRequest {
		const query: any = {}
		const params = new URLSearchParams(queryString)
		const entries = [...params.entries()]
		entries.forEach(([key, value]) => query[key] = decodeURIComponent(value).split(','))

		Object.entries(this.props.activeFilters || {})
			.forEach(([key, value]) => {
				const isArray = Array.isArray(value)
				if (query[key])
					query[key] = isArray ? query[key] : query[key][0]
			})

		if (query.page) {
			query.pagination_data = { page_number: query.page[0] }
			query.page = undefined
		}

		return query
	}

	updateQueryParams(filterGroups: FilterGroup[]) {
		const query: { [key: string]: any[] } = {}

		filterGroups.forEach(group => {
			group.filters.forEach(function getActives(filter) {
				if (filter.subItems.some(getActives) || !filter.active)
					return false

				query[group.key] = query[group.key] || []
				query[group.key].push(filter.id)

				return true
			})
		})

		const search = Object.entries(query).map(([key, value]) => key + '=' + encodeURIComponent(value.join(','))).join('&')
		this.props.history.push({ search })
	}

	render() {
		const groups = this.state.filterGroups
		const loader = this.state.loading ? <div>Pretty loading animation</div> : null

		return (
			<div>
				{loader}

				{groups.map(group =>
					<div key={group.key}>
						<p>{group.name}</p>
						{group.filters.map(filter =>
							filter.subItems.length ?
								<FilterDropdown
									filterId={filter.id}
									name={filter.name}
									active={filter.active}
									subItems={filter.subItems}
									key={filter.id}
									onFilter={filterId => this.toggleActiveFilter(group.key, filterId)}
								/>
								:
								<FilterItem
									name={filter.name}
									active={filter.active}
									key={filter.id}
									onClick={() => this.toggleActiveFilter(group.key, filter.id)}
								/>
						)}
					</div>
				)}
			</div>
		)
	}
}

type MappedProps = {
	addFilter: (key: string, value: string) => void
	removeFilter: (key: string, value: string) => void
}

type StateProps = {
	loading?: boolean
	activeFilters?: MakerState['filters']
}

const mapDispatchToProps = (dispatch: Function) => ({
	addFilter: (key, value) => dispatch(addFilter(key, value)),
	removeFilter: (key, value) => dispatch(removeFilter(key, value)),
} as MappedProps)

const mapStateToProps: MapStateToProps<StateProps, StateProps, StoredState> = state => ({
	loading: state.makers.loading,
	activeFilters: state.makers.filters,
})

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(FilterList))
