import React, { HTMLProps, Component } from 'react'
import { Filter } from '../FilterList/FilterList';
import FilterItem from '../FilterItem/FilterItem';

type Props = {
	name: string
	active: boolean
	subItems: Filter[]
	filterId: string
	onFilter: (filterId: string) => void
} & HTMLProps<HTMLButtonElement>

class FilterDropdown extends Component<Props> {
	state = {
		isDropdownActive: false
	}

	activateDropdown() {
		this.setState({ isDropdownActive: true })
	}

	inactivateDropdown() {
		this.setState({ isDropdownActive: false })
	}

	render() {
		const {
			name,
			active,
			subItems,
			onFilter,
			filterId,
		} = this.props

		const isActive = subItems.some(item => item.active) || this.state.isDropdownActive

		return (
			<div>
				<FilterItem
					name={name}
					active={active}
					onClick={() => {
						onFilter(filterId), this.activateDropdown()
					}}
				/>

				{isActive ?
					<div className="pl1">
						{subItems.map(filter =>
							filter.subItems.length ?
								<FilterDropdown
									filterId={filter.id}
									name={filter.name}
									active={filter.active}
									subItems={filter.subItems}
									key={filter.id}
									onFilter={onFilter}
								/>
								:
								<FilterItem
									name={filter.name}
									active={filter.active}
									key={filter.id}
									onClick={() => onFilter(filter.id)}
								/>
						)}

						<FilterItem
							name="Show less"
							active={false}
							onClick={() => this.inactivateDropdown()}
						/>
					</div>
					: null}
			</div>
		)
	}
}

export default FilterDropdown
