import React, { JSXElementConstructor, HTMLProps } from 'react'

type Props = {
	name: string
	active: boolean
} & HTMLProps<HTMLButtonElement>

const FilterItem: JSXElementConstructor<Props> = ({
	name,
	active,
	onClick,
}) => {
	const classes = ['db', 'w-100', 'ttu', 'bn', 'ph1', 'pv2', 'f6', 'tl', 'mb1', 'pointer', 'dim']

	classes.push(active ? 'bg-light-yellow' : 'bg-transparent')

	return (
		<button type="button" onClick={onClick} className={classes.join(' ')}>
			{name}
		</button>
	)
}

export default FilterItem
