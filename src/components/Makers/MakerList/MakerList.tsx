import React, { Component } from 'react'
import { withRouter, RouteComponentProps } from 'react-router';
import { connect, MapStateToPropsParam } from 'react-redux';
import { Maker } from '../../../entities/Maker';
import MakerItem from '../MakerItem/MakerItem';
import { StoredState } from '../../../store/store';

type Props = StateProps & RouteComponentProps

class MakerList extends Component<Props> {
	render() {
		const makers = this.props.makers || []
		console.log(this.props);
		const loader = !makers.length ? 'Fancy loader' : null

		return (
			<div className="pl2">
				{loader}
				{makers.map(maker =>
					<MakerItem
						key={maker.id}
						name={maker.name}
						description={maker.shortDescription}
						image={maker.mainImage}
						raw={maker.raw}
					/>
				)}
			</div>
		)
	}
}


type StateProps = { makers?: Maker[] }
const mapStateToProps: MapStateToPropsParam<StateProps, StateProps, StoredState> = state => ({
	makers: state.makers.data,
})

export default withRouter(connect(mapStateToProps)(MakerList))

