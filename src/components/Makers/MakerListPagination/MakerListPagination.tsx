import React, { JSXElementConstructor } from 'react'
import { withRouter, RouteComponentProps } from 'react-router';
import { connect, MapStateToPropsParam, MapDispatchToProps, MapDispatchToPropsFunction } from 'react-redux';
import Button from '../../UI/Button/Button';
import { Optional } from '../../../entities/Proto';
import { updatePage, addFilter } from '../../../store/actions';
import { StoredState } from '../../../store/store';

type Props = Optional<DispatchState> & RouteComponentProps & StateProps

const MakerListPagination: JSXElementConstructor<Props> = ({
	select = () => { },
	addFilter = () => { },
	history,
	pageCount = 1,
	currentPage = 1,
}) => {
	let pages: Page[] = [
		{
			current: true,
			text: currentPage.toString(),
			number: currentPage,
		},
	]

	if (currentPage > 1)
		pages.unshift({
			text: (currentPage - 1).toString(),
			number: currentPage - 1,
		})

	if (currentPage < pageCount)
		pages.push({
			text: (currentPage + 1).toString(),
			number: currentPage + 1,
		})

	pages.unshift({
		text: '<',
		number: currentPage - 1,
		disabled: currentPage - 1 == 0
	})

	pages.push({
		text: '>',
		number: currentPage + 1,
		disabled: currentPage + 1 > pageCount
	})

	const selectPage = (page: number) => {
		const query: { [key: string]: string } = {}
		const params = new URLSearchParams(history.location.search)
		const entries = [...params.entries()]
		entries.forEach(([key, value]) => query[key] = decodeURIComponent(value))
		query.page = page.toString()
		const search = Object.entries(query).map(([key, value]) => key + '=' + encodeURIComponent(value)).join('&')
		history.push({ search })

		select(page)
		addFilter(`pagination_data`, { page_number: page })
	}

	return (
		<div className="tc">
			{pages.map(page =>
				<Button key={page.text} onClick={() => selectPage(page.number || currentPage)} active={!!page.current} disabled={page.disabled}>
					{page.text}
				</Button>
			)}
		</div>
	)
}

type Page = {
	text: string
	number?: number
	current?: boolean
	disabled?: boolean
}

type StateProps = {
	pageCount?: number
	currentPage?: number
}

const mapStateToProps: MapStateToPropsParam<StateProps, StateProps, StoredState> = state => ({
	pageCount: state.makers.pagination.maxPages,
	currentPage: state.makers.pagination.currentPage,
})

type DispatchState = {
	select: (page: number) => void
	addFilter: (key: string, value: object) => void
}
const mapDispatchToProps = (dispatch: Function) => ({
	addFilter: (key, value) => dispatch(addFilter(key, value)),
	select: page => dispatch(updatePage(page)),
} as DispatchState)

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(MakerListPagination))
