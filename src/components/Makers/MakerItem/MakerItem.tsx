import React, { JSXElementConstructor, ReactElement } from 'react'

import styles from './MakerItem.module.scss'
import Button from '../../UI/Button/Button';
import { connect, MapDispatchToProps } from 'react-redux';
import { toggleModal } from '../../../store/actions';
import { Brand } from '../../../entities/Proto';

type Props = {
	name: string
	description: string
	image: string
	raw: Brand
} & StateProps

const MakerItem: JSXElementConstructor<Props> = ({
	name,
	description,
	image,
	showModal,
	raw,
}) =>
	<div className="flex flex-auto ba mb2 pa1">
		<div className={styles.Image} style={{ backgroundImage: `url('${image}')` }} />
		<div className={"pl3 " + styles.Text}>
			<h2>{name}</h2>
			<h3>{description}</h3>
		</div>
		<div className={styles.ButtonBox}>
			{!showModal ? null :
				<Button
					className="shadow-4 m2 ph3"
					onClick={() => showModal(
						<div>
							<h2>{raw.name}</h2>
							<p>{raw.description}</p>
							<p>Likes: {raw.likes}</p>
							<p>FB Followers: {raw.facebook_followers}</p>
							<p>Instagram Followers: {raw.instagram_followers}</p>

							<h3>Specification</h3>
							{raw.organic ? <p>Organic</p> : null}
							{raw.eco_friendly ? <p>Eco Friendly</p> : null}
							{raw.is_new ? <p>New</p> : null}

							<h3>Images</h3>
							{raw.images.map(img =>
								<img src={img.url} height={img.height} width={img.width} />
							)}
						</div>
					)}
				>
					More
				</Button>
			}
		</div>
	</div>


type StateProps = {
	showModal?: (content: ReactElement) => void
}

const mapDispatchToProps: MapDispatchToProps<StateProps, Props> = dispatch => ({
	showModal: content => dispatch(toggleModal(true, content))
})

export default connect(null, mapDispatchToProps)(MakerItem)
