import React, { JSXElementConstructor, HTMLProps } from 'react'

type Props = {
	active?: boolean
} & HTMLProps<HTMLButtonElement>

const Button: JSXElementConstructor<Props> = props => {
	const {
		active,
		className,
	} = props

	const classes = ['dib', 'ttu', 'bn', 'ph1', 'pv2', 'f6', 'tc', 'pointer', 'dim']

	if (active) {
		classes.push('bg-blue')
		classes.push('white')
	}

	const buttonProps: any = {
		...props,
	}

	delete buttonProps.active
	delete buttonProps.className

	return (
		<button type="button" className={classes.join(' ') + ` ` + (className || '')} {...buttonProps}>
			{props.children}
		</button>
	)
}

export default Button
