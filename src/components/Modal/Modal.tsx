import React, { ReactElement, JSXElementConstructor } from 'react'
import { connect, MapStateToProps, MapDispatchToProps } from 'react-redux';
import { StoredState } from '../../store/store';
import Button from '../UI/Button/Button';

import styles from './Modal.module.scss'
import { toggleModal } from '../../store/actions';

type Props = StateProps & DispatchProps

const Modal: JSXElementConstructor<Props> = ({
	show,
	content,
	hideModal = () => null,
}) =>
	!show ? null :
		<div className={styles.Container}>
			<div className={styles.Modal}>
				<Button className={'shadow-2 round ph2 ' + styles.ModalButton} onClick={() => hideModal()}>
					X
				</Button>
				<div>
					{content}
				</div>
			</div>
			<div className={styles.Backdrop} onClick={() => hideModal()} />
		</div>

type StateProps = {
	show?: boolean,
	content?: ReactElement | null,
}

const mapStateToProps: MapStateToProps<StateProps, Props, StoredState> = state => ({
	show: state.modal.show,
	content: state.modal.content,
})


type DispatchProps = {
	hideModal?: () => void
}

const mapDispatchToProps: MapDispatchToProps<DispatchProps, Props> = dispatch => ({
	hideModal: () => dispatch(toggleModal(false, null))
})

export default connect(mapStateToProps, mapDispatchToProps)(Modal)