/* React imports */
import React, { Component } from 'react'
import Header from './components/Header/Header';
import { Switch, Route, Redirect, withRouter, RouteComponentProps } from 'react-router-dom';
import Makers from './routes/Makers/Makers';
import Footer from './components/Footer/Footer';
import Modal from './components/Modal/Modal';

type Props = RouteComponentProps

class App extends Component<Props> {
	render() {
		return (
			<div className="flex flex-column justify-between min-vh-100">
				<Header></Header>

				<main className="flex-auto self-center pt1 pb4 ph3 mw8 w-100">
					<Switch>
						<Route path="/makers/" component={Makers} />
						<Redirect path="/" to="/makers" />
					</Switch>
				</main>

				<Footer />
				<Modal />
			</div>
		);
	}
}

export default withRouter(App)
